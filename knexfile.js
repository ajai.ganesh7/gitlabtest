const dotenv = require('dotenv')
dotenv.config();

module.exports = {
  development: {
    client: process.env.CLIENT,
    connection:{
      host : process.env.HOST,
      user : process.env.USER,
      password : process.env.PASSWORD,
      database : process.env.DB
    },
    migrations: {
      directory: __dirname + '/db/migrations'
    },
    seeds: {
      directory: __dirname + '/db/seeds/dev'
    },
    useNullAsDefault: true
  },
}