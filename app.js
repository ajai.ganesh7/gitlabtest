const dotenv = require('dotenv')
dotenv.config()
const knex = require('./db/knex');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.PORT
const morgan = require('morgan')

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}))

app.use(morgan('dev'))
app.use(cors())

require('./route')(app)

app.listen(port)
console.log('the server is running on '+port);



