
exports.up = function(knex, Promise) {
    return knex.schema.createTable('links', function(table){
        table.increments().notNullable();
        table.string('links').notNullable()
        table.string('linkName').notNullable()
        table.json('users_id')
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
  };
  
  exports.down = function(knex, Promise) {
      return knex.schema.dropTable('links');
  };
  