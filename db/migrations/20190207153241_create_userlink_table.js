
exports.up = function(knex, Promise) {
  return knex.schema.createTable('userlink', function(table){
      table.increments().notNullable();
      table.integer('users_id').references('id').inTable('users');
      table.integer('links_id').references('id').inTable('links');
  })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('userlink');        
};
