const registorController = require('./db/controllers/registorController')
const linkController = require('./db/controllers/linkController')
const loginController = require('./db/controllers/loginController')

module.exports = function(router){
    router.post('/api/registor', registorController.registor)
    router.get('/api/getRegistor', registorController.getRegistor)
    router.post('/api/addLink', linkController.addLink)
    router.get('/api/getLink', linkController.getLink)
    router.post('/api/login', loginController.login)
}